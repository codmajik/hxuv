
#include "nuv.h"


DEFINE_KIND(k_nuv_timer);

typedef struct {
  uv_timer_t  timer;
  value       func;
} nuv_timer;

static void __gc_timer(value k) {
  val_gc(k, 0);
  NEKO_UV_CLOSE_HANDLE(val_data(k));
}

static void __hx_nuv_timer_callback(uv_timer_t *handle, int status) {
  val_call0(((nuv_timer*)handle)->func);
}

value hx_nuv_timer_init(value func) {

  val_check_function(func, 0);

  uv_loop_t* loop = __nuv_get_vm_loop();

  nuv_timer* vt = (nuv_timer*)alloc_private( sizeof(nuv_timer) );
  uv_timer_init( loop, &vt->timer );
  vt->func = func;

  RETURN_ABSTRACT(k_nuv_timer, vt, __gc_timer);
}

value hx_nuv_timer_start(value handle, value timeout, value repeat) {

  val_check_kind(handle, k_nuv_timer);

  THROW_EXCEPTION( val_is_int(timeout), "Invalid Timeout Value" );
  THROW_EXCEPTION( val_is_int(repeat), "Invalid Repeat Value" );

  uv_timer_start(&((nuv_timer*)val_data(handle))->timer,
          __hx_nuv_timer_callback, val_int(timeout), val_int(repeat));

  return val_true;
}

value hx_nuv_timer_stop(value handle) {

  val_check_kind(handle, k_nuv_timer);

  int t = uv_timer_stop( &((nuv_timer*)val_data(handle))->timer );
  THROW_EXCEPTION(t == 0, "Invalid Timer!!!");

  return val_true; // everything was ok
}

value hx_nuv_timer_again(value handle) {

  val_check_kind(handle, k_nuv_timer);

  int t = uv_timer_again( &((nuv_timer*)val_data(handle))->timer );
  THROW_EXCEPTION(t == 0, "Invalid Timer!!!");

  return val_true; // everything was ok
}


DEFINE_PRIM(hx_nuv_timer_init, 1);
DEFINE_PRIM(hx_nuv_timer_start, 3);
DEFINE_PRIM(hx_nuv_timer_stop, 1);
DEFINE_PRIM(hx_nuv_timer_again, 1);
