

#ifndef _H_HX_NUV

#define _H_HX_NUV

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

#include "uv.h"
#include "neko_vm.h"

uv_loop_t *__nuv_get_vm_loop();

#define NEKO_UV_CLOSE_HANDLE_CB(X,CB) { \
  if(uv_is_active((uv_handle_t*)X) && !uv_is_closing((uv_handle_t*)X)) \
    uv_close((uv_handle_t*)X, CB);}

#define NEKO_UV_CLOSE_HANDLE(X) \
  NEKO_UV_CLOSE_HANDLE_CB(X,NULL)

#define SET_UV_ERROR(l) \
  uv_err_t e = uv_last_error(l->loop); \
  param[1] = alloc_int( e.code ); \
  param[2] = alloc_string(uv_err_name(e) );


#define THROW_EXCEPTION(cond, msg) \
  if (!(cond) ) { val_throw(alloc_string(msg)); \
  return val_null; }


#define ALLOC_MEM(sz) alloc(sz);

#define RETURN_ABSTRACT_NO_GC(k, o) \
  return alloc_abstract(k, o);

#define RETURN_ABSTRACT(k, o, gc) \
  value v = alloc_abstract(k, o); \
  val_gc(v, gc); \
  return v;

#endif //_H_HX_NUV
