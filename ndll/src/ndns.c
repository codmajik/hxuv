
// implement libuv based dns lookup

#include "nuv.h"

typedef struct {
  uv_getaddrinfo_t resolver;
  addrinfo hints;
  value callback;
} vdns;


static void on_resolved(uv_getaddrinfo_t *resolver, int status, struct addrinfo *res) {

  static char addr[17] = {'\0'};

  value errcode = val_null, errmsg = val_null, ipaddr = val_null;


  vdns* dns = (vdns*)resolver;
  if (status == -1) {
    uv_err_t e = uv_last_error( __nuv_get_vm_loop() );
    errcode = alloc_int( e.code );
    errmsg = alloc_string( uv_err_name(e) );
  } else {
    uv_ip4_name((struct sockaddr_in*) res->ai_addr, addr, 16);
    ipaddr = alloc_string(addr);
  }

  val_call3(dns->callback, ipaddr, errcode, errmsg);
  uv_freeaddrinfo(res);
}


value hx_nuv_dns_resolve(value host, value func, value service) {


  THROW_EXCEPTION( val_is_string(host),  "Invalid Host");
  val_check_function(func, 3);

  uv_loop_t * loop = __nuv_get_vm_loop();

  vdns* dns_h = (vdns*)calloc( sizeof(vdns), 1 );
  dns_h->hints.ai_family = PF_INET;
  dns_h->hints.ai_socktype = SOCK_STREAM;
  dns_h->hints.ai_protocol = IPPROTO_TCP;
  dns_h->hints.ai_flags = 0;

  dns_h->callback = func;

  int r = uv_getaddrinfo(loop, &dns_h->resolver, on_resolved,
                        val_string(host), val_string(service), &dns_h->hints);

  if( r ) free(dns_h);
  THROW_EXCEPTION(r == 0, uv_err_name(uv_last_error(loop)));

  return val_true;
}

DEFINE_PRIM( hx_nuv_dns_resolve, 3 );