
#include "nuv.h"

DEFINE_KIND(k_nuv_loop);

value hx_nuv_loop_shutdown();

uv_loop_t *__nuv_get_vm_loop() {
  return (uv_loop_t*)neko_vm_custom(neko_vm_current(), k_nuv_loop);
}

static void __nuv_gc_loop(value k) {
  hx_nuv_loop_shutdown();
  val_gc(k, 0);
}

static void __nuv_shutdown_loop(uv_handle_t* handle, void* arg) {
  NEKO_UV_CLOSE_HANDLE(handle);
}

// Initialize per-thread loop
// we don't expect operating single loop in multiple
// thread. Users would mostly use the bg work system to handle processing
// but per-thread makes it possible to write a multi-loop
// multi-service systems
value hx_nuv_loop_init (value type_) {

  // alloc loop object for this thread
  // ofcourse only if one doesn't exists already
  uv_loop_t* loop = __nuv_get_vm_loop();
  neko_vm* vm = neko_vm_current();


  if( !loop )
    neko_vm_set_custom(vm , k_nuv_loop, (loop = uv_loop_new()));

  //uv_ref((uv_handle_t*)loop);

  value v = alloc_abstract(k_nuv_loop, loop);
  val_gc(v, __nuv_gc_loop);

  return v;
}

value hx_nuv_loop_run () {
  return alloc_int( uv_run( __nuv_get_vm_loop(), UV_RUN_DEFAULT ) );
}

value hx_nuv_loop_run_once () {
  return alloc_int( uv_run( __nuv_get_vm_loop(), UV_RUN_ONCE ) );
}


value hx_nuv_loop_shutdown () {

  uv_loop_t * loop = __nuv_get_vm_loop();

  neko_vm_set_custom( neko_vm_current() , k_nuv_loop, NULL );

  // detach all handles from this loop
  uv_walk( loop, __nuv_shutdown_loop, neko_vm_current());
  uv_loop_delete( loop );

  return alloc_bool( true );
}

DEFINE_PRIM( hx_nuv_loop_init, 0);
DEFINE_PRIM( hx_nuv_loop_run, 0);
DEFINE_PRIM( hx_nuv_loop_run_once, 0);
DEFINE_PRIM( hx_nuv_loop_shutdown, 0);
