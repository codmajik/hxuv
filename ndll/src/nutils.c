
#include "nuv.h"


value hx_nuv_get_cpu_info () {


  uv_cpu_info_t* cpus;
  int count;

  uv_err_t err = uv_cpu_info( &cpus, &count );
  THROW_EXCEPTION( err.code == 0, "GetCPUInfo Failed" );

  value arr = alloc_array( count );

  for( int idx = 0; idx < count; idx++ ) {

    uv_cpu_info_t cpu = cpus[idx];
    value obj = val_array_ptr(arr)[idx] = alloc_object(val_null);
    alloc_field(obj, val_id("model"), alloc_string(cpu.model));
    alloc_field(obj, val_id("speed"), alloc_float(cpu.speed));
  }

  uv_free_cpu_info(cpus, count);
  return arr;
}


value hx_nuv_get_network_interfaces() {

  char buf[512];
  int count;

  uv_interface_address_t *info;

  uv_interface_addresses( &info, &count );

  value arr = alloc_array( count );

  for( int idx = 0; idx < count; idx++ ) {

    uv_interface_address_t iface = info[idx];

    value obj = val_array_ptr(arr)[idx] = alloc_object(val_null);
    alloc_field(obj, val_id("name"), alloc_string(iface.name));
    alloc_field(obj, val_id("internal"), iface.is_internal ? val_true : val_false );


    int ver = 0;
    switch(iface.address.address4.sin_family) {
      case AF_INET:
        ver = 4;
        uv_ip4_name( &iface.address.address4, buf, sizeof(buf) );
        break;

      case AF_INET6:
        ver = 6;
        uv_ip6_name( &iface.address.address6, buf, sizeof(buf) );
        break;
    }

    alloc_field( obj, val_id("version"), alloc_int(ver));
    alloc_field( obj, val_id("ip"), alloc_string(ver ? buf : "Unknown") );
  }

  uv_free_interface_addresses(info, count);

  return arr;
}

DEFINE_PRIM( hx_nuv_get_cpu_info, 0);
DEFINE_PRIM( hx_nuv_get_network_interfaces, 0);