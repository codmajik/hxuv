

#include "nuv.h"

typedef struct {
  value callback;
  int nargs;
} ds_stream;

typedef struct {
  uv_any_handle stream;
  ds_stream     ctx_write;
  ds_stream     ctx_read;
  ds_stream     ctx_closed;
  ds_stream     ctx_extra;

  value         _this;
  bool         busy;
  bool         is_pipe;
} vstream;


typedef struct {
  uv_write_t  request;
  uv_buf_t     buf;
  uv_stream_t* s;
} nuv_write_req;

DEFINE_KIND(k_nuv_stream);


#define DEFAULT_READ_SIZE 1024

#define EXEC_CALLBACK \
  value except = val_null; \
  val_callEx(vstrm->_this, ctx.callback, param+(3-ctx.nargs), ctx.nargs, &except); \
  if ( except != val_null ) {\
    printf("\n%s Callback(%d) Exception on %s:%d\n",__FUNCTION__, ctx.nargs,__FILE__, __LINE__);\
    val_print(except);\
  }

#define GET_CONTEXT_INFO(X,c) \
  vstream* vstrm = ((vstream*)X->data);\
  ds_stream ctx = vstrm->ctx ## _ ## c; \
  value param[3] = {/* data */ val_null, \
                    /* error code */ val_null, \
                    /* error message */ val_null };

#define GET_WRITE_CONTEXT_INFO(X) \
  GET_CONTEXT_INFO(X, write)

#define GET_READ_CONTEXT_INFO(X) \
  GET_CONTEXT_INFO(X, read)

#define GET_EXTRA_CONTEXT_INFO(X) \
  GET_CONTEXT_INFO(X, extra)

#define GET_CLOSE_CONTEXT_INFO(X) \
  GET_CONTEXT_INFO(X, closed)

#define STREAM_CLOSE(X) \
  NEKO_UV_CLOSE_HANDLE_CB(X,__hx_nuv_client_on_closed);


inline static vstream* alloc_vstream( size_t sz = 65536 ) {
  vstream* ds = (vstream*)ALLOC_MEM( sizeof(vstream) /*+ sz */);
  ds->stream.stream.data = (void*)ds;
  return ds;
}

static uv_buf_t _hx_nv_alloc_buf(uv_handle_t *handle, size_t suggested_size) {
  static char slab[65536];
  return uv_buf_init(slab, sizeof(slab));
}

static void __nuv_gc_stream(value k) {
  val_gc(k, 0);
  NEKO_UV_CLOSE_HANDLE( &((vstream*)val_data(k))->stream.handle );
}


static void __hx_nuv_client_on_closed(uv_handle_t* handle) {

  GET_CLOSE_CONTEXT_INFO(handle);

  // callback_func( )
  EXEC_CALLBACK;
}

static void __hx_nuv_server_on_connect_request(uv_stream_t *server, int status) {

  GET_EXTRA_CONTEXT_INFO(server);

  if (status == -1) {
    SET_UV_ERROR( server );
    STREAM_CLOSE( server );
  }

  else {
    vstream* client = alloc_vstream( DEFAULT_READ_SIZE );
    if( vstrm->is_pipe ) uv_pipe_init(server->loop, &client->stream.pipe, 0);
    else uv_tcp_init( server->loop, &client->stream.tcp );

    if (uv_accept( server, &client->stream.stream) == 0) {
      param[0] = alloc_abstract(k_nuv_stream, client);

      //attach close handle to vstream
      client->stream.handle.close_cb = __hx_nuv_client_on_closed;
      val_gc(param[0], __nuv_gc_stream);
    }
    else STREAM_CLOSE( &client->stream.handle );
  }

  // callback(?ClientSocket, ?errcode, ?errmsg):Void
  EXEC_CALLBACK;
}

static void __hx_nuv_client_on_connected(uv_connect_t *client, int status) {

  GET_EXTRA_CONTEXT_INFO(client);

  if (status == -1) {
    SET_UV_ERROR(client->handle);
    STREAM_CLOSE(client->handle);
  } else {
    //attach close handle to vstream
    client->handle->close_cb = __hx_nuv_client_on_closed;
  }

  // callback_func( errcode:Int, errmsg:String )
  EXEC_CALLBACK;
}

static void __hx_nuv_stream_read_complete(uv_stream_t *client, ssize_t nread, uv_buf_t buf) {

  GET_READ_CONTEXT_INFO(client);

  //buffer b = alloc_buffer(NULL);
  //buffer_append_sub(b,pd,sz);

  if( nread == -1 ) {
    SET_UV_ERROR(client);
    STREAM_CLOSE(client)
  }
  else if(nread > 0) param[0] = copy_string( buf.base, nread );

  // callback(data:String, errcode:Int, errmsg:String):Void
  EXEC_CALLBACK;
}

static void __hx_nuv_stream_write_complete(uv_write_t *req, int status) {

  GET_WRITE_CONTEXT_INFO(req);

  if( status == -1 ) {
    SET_UV_ERROR(req->handle);
    STREAM_CLOSE(req->handle);
  }

  //  callback(errcode:Int, errmsg:String):Void
  EXEC_CALLBACK;

  free((nuv_write_req*)req);
}

/**
 * Read Data from Stream
 * @param stream  -> stream object (kind k_stream) initiatiated with one of init.
 * @param nread   -> number of bytes to read (int).
 * @param func -> function to call when operation is completed.
 * @param ctx     -> context (the this object) for the function (if NULL its a global lambda).
 **/
value hx_nuv_stream_read (value stream) {

  val_check_kind(stream, k_nuv_stream);

  vstream* vstrm = (vstream*)val_data(stream);
  if( vstrm->busy ) return val_false;

  int rc = uv_read_start((uv_stream_t*)&vstrm->stream,
                              _hx_nv_alloc_buf, __hx_nuv_stream_read_complete);
  THROW_EXCEPTION(rc == 0, "Start Read Failed");

  vstrm->busy = true;

  return val_true;
}


value hx_nuv_stream_read_stop (value stream) {

  val_check_kind(stream, k_nuv_stream);


  int rc = uv_read_stop(&((vstream*)val_data(stream))->stream.stream);
  THROW_EXCEPTION(rc == 0, "Stop Read Failed");

  ((vstream*)val_data(stream))->busy = false;
  return val_true;
}

value hx_nuv_stream_close (value stream) {

  val_check_kind(stream, k_nuv_stream);

  uv_any_handle* client = &(*((vstream*)(val_data(stream)))).stream;

  if(uv_is_active(&client->handle) && !uv_is_closing(&client->handle))
    uv_close(&client->handle, __hx_nuv_client_on_closed);

  //close(client->stream.io_watcher.fd);

  return val_true;
}

/**
 * Write Data to Stream
 * @param stream  -> stream object (kind k_stream) initiatiated with one of init.
 * @param msg    -> string/byte data to write.
 **/
value hx_nuv_stream_write (value stream, value msg, value send_handle) {

  val_check_kind( stream, k_nuv_stream );

  uv_stream_t* s_handle = NULL;
  if(send_handle != val_null) {
    val_check_kind( send_handle, k_nuv_stream );
    s_handle = (uv_stream_t*)val_data(send_handle);
  }

  THROW_EXCEPTION( val_is_string(msg), "stream->write: string expected" );

  // we dont want to queue a async io call for zero bytes
  if( val_strlen(msg) < 1 ) return val_false;

  nuv_write_req* req = (nuv_write_req*)malloc(sizeof(nuv_write_req));
  req->request.data = val_data(stream);
  req->s = &((vstream*)val_data(stream))->stream.stream;

  req->buf.base = val_string(msg);
  req->buf.len = val_strlen(msg);

  int rc = uv_write2(
            &req->request, req->s, &req->buf, 1,
                s_handle, __hx_nuv_stream_write_complete);

  THROW_EXCEPTION(rc == 0, "stream->write: Request Failed");

  return val_true;
}


/* HELPER FUNCTION */
value hx_nuv_stream_init_io_cb(value stream,
    value read_func,value write_func, value closed_func, value ctx) {

  val_check_kind( stream, k_nuv_stream );


  val_check_function(read_func, 3);
  val_check_function(write_func, 2);
  val_check_function(closed_func, 0);

  vstream* strm = (vstream*)val_data(stream);

  strm->_this = ctx;
  strm->ctx_read.nargs = 3;
  strm->ctx_read.callback = read_func;

  strm->ctx_write.nargs = 2;
  strm->ctx_write.callback = write_func;

  strm->ctx_closed.nargs = 0;
  strm->ctx_closed.callback = closed_func;

  return stream;
}

/**
 * Connect to TCP Server
 * @param host_addr   ->  DNS Name / IP Address
 * @param port        ->  Port
 * @param func     -> function to call when operation is completed.
 * @param ctx         -> context (the this object) for the function (if NULL its a global lambda).
 **/
value hx_nuv_stream_tcp_connect(value host_addr, value port, value func, value ctx) {

  THROW_EXCEPTION(val_is_string(host_addr),"expected string for host address");
  THROW_EXCEPTION(val_is_int(port),"expected int for port");

  val_check_function(func, 2);

  // start action
  sockaddr_in dest = uv_ip4_addr(val_string(host_addr), val_int(port));

  vstream* client = alloc_vstream( DEFAULT_READ_SIZE );
  uv_tcp_init(__nuv_get_vm_loop(), &client->stream.tcp);

  client->_this = ctx;

  client->ctx_extra.nargs = 2;
  client->ctx_extra.callback = func;

  uv_connect_t* connect = (uv_connect_t*) ALLOC_MEM( sizeof(uv_connect_t) );
  connect->data = client;
  client->is_pipe = false;

  int rc = uv_tcp_connect(connect, &client->stream.tcp,
                                          dest, __hx_nuv_client_on_connected);

  THROW_EXCEPTION(rc == 0, "Connect Failed!!!");

  RETURN_ABSTRACT(k_nuv_stream, client, __nuv_gc_stream);
}

value hx_nuv_stream_tcp_get_name(value stream, value peer) {

  val_check_kind( stream, k_nuv_stream );

  struct sockaddr_in name;
  int namelen = sizeof(sockaddr_in);

  static char addr[17] = {'\0'};

  vstream* vstrm = (vstream*)val_data(stream);

  int ret;
  if( val_bool(peer) )
    ret = uv_tcp_getpeername(&vstrm->stream.tcp, (sockaddr*)&name, &namelen);
  else ret = uv_tcp_getsockname(&vstrm->stream.tcp, (sockaddr*)&name, &namelen);

  THROW_EXCEPTION(ret == 0, "Get Name Failed");

  uv_ip4_name(&name, addr, 16);

  value o = alloc_object(val_null);
  alloc_field(o, val_id("ip"), alloc_string(addr));
  alloc_field(o, val_id("port"), alloc_int((unsigned)ntohs(name.sin_port)));

  return o;
}


/**
 * Create to TCP Server
 * @param host_addr   ->  Interface IP Address
 * @param port        ->  Port to listen on
 * @param func     -> function to call when a client connects.
 * @param ctx         -> context (the this object) for the function (if NULL its a global lambda).
 **/
value hx_nuv_stream_tcp_listen(value host_addr, value port, value func, value ctx) {

  THROW_EXCEPTION( val_is_string(host_addr),  "Invalid Host Address");
  THROW_EXCEPTION( val_is_int(port), "expected int for port");

  val_check_function( func, 3 );

  sockaddr_in bind_addr = uv_ip4_addr( val_string(host_addr), val_int(port) );

  vstream* server = alloc_vstream(0);
  uv_tcp_init(__nuv_get_vm_loop(), &server->stream.tcp);
  uv_tcp_bind( &server->stream.tcp, bind_addr );

  server->is_pipe = false;

  server->_this = ctx;
  server->ctx_extra.nargs = 3;
  server->ctx_extra.callback = func;

  int rc = uv_listen( (uv_stream_t*)&server->stream.stream, 128,
                                          __hx_nuv_server_on_connect_request);
  THROW_EXCEPTION(rc == 0, "Listen Failed!!!");

  RETURN_ABSTRACT(k_nuv_stream, server, __nuv_gc_stream);
}


value hx_nuv_stream_pipe_connect(value path, value func, value ctx) {

  THROW_EXCEPTION(val_string(path),"Invalid Pipe");

  val_check_function(func, 2);

  uv_loop_t* loop = __nuv_get_vm_loop();

  // start action
  vstream* client = alloc_vstream( DEFAULT_READ_SIZE );
  uv_pipe_init(loop, &client->stream.pipe, 0);

  client->_this = ctx;
  client->is_pipe = true;
  client->ctx_extra.nargs = 2;
  client->ctx_extra.callback = func;


  uv_connect_t* connect = (uv_connect_t*) ALLOC_MEM( sizeof(uv_connect_t) );
  connect->data = client;

  uv_pipe_connect(connect,
                    &client->stream.pipe,
                          val_string(path),
                            __hx_nuv_client_on_connected);

  RETURN_ABSTRACT(k_nuv_stream, client, __nuv_gc_stream);

}

value hx_nuv_stream_pipe_listen(value path, value func, value ctx) {

  THROW_EXCEPTION( val_is_string(path),  "Invalid Pipe Path");
  val_check_function( func, 3 );

  uv_loop_t* loop = __nuv_get_vm_loop();

  vstream* server = alloc_vstream(0);
  uv_pipe_init(loop, &server->stream.pipe, 0);

  server->is_pipe = true;

  int r = uv_pipe_bind(&server->stream.pipe, val_string(path));
  THROW_EXCEPTION(!r, uv_err_name(uv_last_error(loop)));


  server->_this = ctx;
  server->ctx_extra.nargs = 3;
  server->ctx_extra.callback = func;

  r = uv_listen(
        (uv_stream_t*)&server->stream.pipe, 128, __hx_nuv_server_on_connect_request);
  THROW_EXCEPTION(!r, uv_err_name(uv_last_error(loop)));


  RETURN_ABSTRACT(k_nuv_stream, server, __nuv_gc_stream)
}



// export primitives to nekovm
DEFINE_PRIM( hx_nuv_stream_read, 1 );
DEFINE_PRIM( hx_nuv_stream_read_stop, 1 );
DEFINE_PRIM( hx_nuv_stream_write, 3 );
DEFINE_PRIM( hx_nuv_stream_close, 1 );
DEFINE_PRIM( hx_nuv_stream_init_io_cb, 5 );

DEFINE_PRIM( hx_nuv_stream_tcp_connect, 4 );
DEFINE_PRIM( hx_nuv_stream_tcp_listen, 4 );
DEFINE_PRIM( hx_nuv_stream_tcp_get_name, 2 );

DEFINE_PRIM( hx_nuv_stream_pipe_connect, 3 );
DEFINE_PRIM( hx_nuv_stream_pipe_listen, 3 );
