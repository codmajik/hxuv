
#include "nuv.h"

DEFINE_KIND(k_nuv_async)

typedef struct {
  uv_async_t  async;
  uv_loop_t*  vloop;
  value       func;
  value       ctx;
} nuv_async;

static void __hx_nuv_async_complete(uv_async_t *handle, int status /*UNUSED*/) {
  nuv_async* async = (nuv_async*)handle;

  value exc = val_null;
  value param[0];

  val_callEx(async->ctx, async->func, param, 0, &exc);
}

value hx_nuv_async_exec(value a) {
  val_check_kind(a, k_nuv_async);

  nuv_async* async = (nuv_async*)val_data(a);
  return alloc_int( uv_async_send( &async->async ) );
}

value hx_nuv_async_init(value func, value ctx) {

  val_check_function(func, 0);

  nuv_async* async = (nuv_async*)alloc_private(sizeof(nuv_async));

  async->vloop = __nuv_get_vm_loop();
  async->func = func;
  async->ctx  = ctx;

  uv_async_init(async->vloop, &async->async, __hx_nuv_async_complete);

  return alloc_abstract(k_nuv_async, async);
}

DEFINE_PRIM( hx_nuv_async_init, 2);
DEFINE_PRIM( hx_nuv_async_exec, 1);
