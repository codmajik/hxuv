
import hxuv.Stream;
import hxuv.Utils.AsyncWorker;
import hxuv.Loop;

class Main {

  public function new(){ }

  public static function fib(a:Int):Int {
    Sys.sleep(6);
    return a == 0 ? 1 : a + fib(a-1);
  }

  public static function main() {

    var http = new TCPClient('127.0.0.1', 9073);

    var echoServer = new TCPServer(9073);
    var pipeServer = new PipeServer("/tmp/testpipe.sock");

    var w = new AsyncWorker();
    w.complete = function(r:Dynamic) {
      trace("Working Complete -> " + r);
    }

    w.work = function(p:Dynamic) {
      return Main.fib(10000);
    }

    pipeServer.onnewclient = function( cl:StreamInterface ) {


      cl.onread = function(data:String) {

        trace("New Pipe Data: " + data + "\t-\t"+ (data == "run-work") + "\n" );
        cl.send(data);
        if(data == "run-work") w.run(data);

      }

      cl.onerror = function(ecode:Int, emsg:String) {
        trace("Pipe Error: [" + ecode + " => " + emsg + "]");
      }

      cl.read();
    }

    echoServer.onnewclient = function( cl:StreamInterface ) {

      //cl.onwrite = function() {
        //trace("Sent");
      //}

      cl.onread = function(data:String) {

        trace("New Data: " + data + "\t-\t"+ (data == "run-work") + "\n" );
        cl.send(data);
        if(data == "run-work") w.run(data);

      }

      cl.onerror = function(ecode:Int, emsg:String) {
        trace("Error: [" + ecode + " => " + emsg + "]");
      }

      cl.read();
    }

    pipeServer.listen();
    echoServer.listen();

    var total_sz:Int = 0;

    http.onconnected = function() {
      http.send("run-work");
    }

    http.onwrite = function() {
      http.read();
    }

    http.onread = function(data:String) {
      total_sz = total_sz + data.length;
      trace("Data+: " + data.length);
    }

    http.onerror = function(ecode:Int, emsg:String) {
      trace("Error: [" + ecode + " => " + emsg + "]");
    }

    http.connect();

    IOLoop.start();
  }
}
