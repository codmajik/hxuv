# hxuv -- [haXe][1]/[NekoVM][2] binding/framework for [libuv][3]

Neko binding for libuv allowing a nodejs style app with neko backend,
This version only implements:

  - `Non-blocking TCP sockets`

  - `Non-blocking named pipes`

  - `AsyncWorker (aka Thread pool scheduling)`

    - `Re-Implemented with neko Threads` 
        -- allows for cpu bound operations to 
           run in neko instread of writting c 
           modules like nodejs

  - `High Res Timers`


On the High-Level provides interface to 
  - ` Async Remoting Stack (Server, Socket, Protocol) `

[1]: http://haxe.org
[2]: http://nekovm.org
[3]: https://github.com/joyent/libuv
