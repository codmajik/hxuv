
package hxuv;

import neko.Lib;
import hxuv.Loop.IOLoop;
import hxuv.Loop.Handle;

private class Stream extends Handle {

  var loop:Dynamic; // Loop We don't need it for anything, just to prevent gc

  public dynamic function onError(ecode:Int, emsg:String):Void {}

  function new() {
    loop = IOLoop.getInstance();
  }

  static var _hx_uv_tcp_connect = Lib.load(IOLoop.LIBNAME, "hx_nuv_stream_tcp_connect", 4);
  static var _hx_uv_tcp_listen = Lib.load(IOLoop.LIBNAME, "hx_nuv_stream_tcp_listen", 4);
  static var _hx_uv_tcp_name = Lib.load(IOLoop.LIBNAME, "hx_nuv_stream_tcp_get_name", 2);

  static var _hx_uv_pipe_connect = Lib.load(IOLoop.LIBNAME, "hx_nuv_stream_pipe_connect", 3);
  static var _hx_uv_pipe_listen = Lib.load(IOLoop.LIBNAME, "hx_nuv_stream_pipe_listen", 3);

  static var _hx_uv_dns_resolve = Lib.load(IOLoop.LIBNAME, "hx_nuv_dns_resolve", 3);

  static var _hx_uv_stream_init_io_cb = Lib.load(IOLoop.LIBNAME, "hx_nuv_stream_init_io_cb", 5);
  static var _hx_uv_stream_read = Lib.load(IOLoop.LIBNAME, "hx_nuv_stream_read", 1);
  static var _hx_uv_stream_read_stop = Lib.load(IOLoop.LIBNAME, "hx_nuv_stream_read_stop", 1);
  static var _hx_uv_stream_close = Lib.load(IOLoop.LIBNAME, "hx_nuv_stream_close", 1);
  static var _hx_uv_stream_write = Lib.load(IOLoop.LIBNAME, "hx_nuv_stream_write", 3);

}

private class StreamIOOperations extends Stream {

  var peerInfo:{ port:Int, host:sys.net.Host };
  var hostInfo:{ port:Int, host:sys.net.Host };

  public var custom:Dynamic;

  dynamic public function onClosed():Void {}
  dynamic public function onDataReady(dt:String):Void {}
  dynamic public function onWriteComplete():Void {}

  public function send(data:String) {
    if( handle == null ) throw "Invalid Stream handle";
    Stream._hx_uv_stream_write(handle, Lib.haxeToNeko(data), null);
    return this;
  }

  public function read() {
    if( handle == null ) throw "Invalid Stream handle";
    Stream._hx_uv_stream_read(handle);
    return this;
  }

  public function pauseRead() {
    if( handle == null ) throw "Invalid Stream handle";
    Stream._hx_uv_stream_read_stop(handle);
    return this;
  }

  public function peer():{ port:Int, host:sys.net.Host } {
    if( handle == null ) throw "Invalid Stream handle";

    if( peerInfo == null ) {
      var info:Dynamic = Stream._hx_uv_tcp_name(handle, true);

      peerInfo = {
        port: Std.parseInt(Std.string(info.port)),
        host: new sys.net.Host(Std.string(info.ip))
      };

    }

    return peerInfo;
  }

  public function host():{ port:Int, host:sys.net.Host } {

    if( handle == null ) throw "Invalid Stream handle";

    if( hostInfo == null ) {
      var info:Dynamic = Stream._hx_uv_tcp_name(handle, false);
      hostInfo = {
        port: Std.parseInt(Std.string(info.port)),
        host: new sys.net.Host(Std.string(info.ip))
      };
    }

    return hostInfo;
  }

  public function close() {

    Stream._hx_uv_stream_close(this.handle);

    // start by not reporting any callback
    onError = null;
    onWriteComplete = null;
    onDataReady = null;
  }

  function on_data( ?data:String, ?errcode:Int, ?errmsg:String ) {
    if( errcode == null )
      onDataReady(data == null ? "" : Lib.nekoToHaxe(data));

    else onError(errcode, errmsg);
  }

  function on_written( ?errcode:Int, ?errmsg:String ) {
    if( errcode == null ) onWriteComplete();
    else onError(errcode, errmsg);
  }

  // stream stuff

}

private class ClientAdapter extends StreamIOOperations {

  var connected:Bool = false;

  public function new( ?s:Dynamic = null ) {
    super();
    this.set_handle(s);
  }

  dynamic public function onConnected():Void {}

  function on_connected(?errcode:Int, ?errmsg:String) {
    if(errcode == null) {
      set_handle(handle);
      onConnected();
    }
    else onError(errcode, errmsg);
  }


  function on_closed() {
    onClosed();
    handle = null; // exec so gc can be called
  }

  function set_handle(s:Dynamic) {
    if(s != null ) {
      connected = true;
      handle = Stream._hx_uv_stream_init_io_cb(s, on_data, on_written, on_closed, this);
    }
  }


}

class TCPClient extends ClientAdapter {

  var server_addr:String;
  var server_port:Int;

  public function new( host:String, port:Int ) {

    super();

    this.server_addr = host;
    this.server_port = port;
  }

  public function connect() {
    if( this.connected ) return;
    Stream._hx_uv_dns_resolve(
      Lib.haxeToNeko(server_addr),callback(on_dns_resolved), null);
  }

  function on_dns_resolved(ip:String, e:Int, emsg:String) {
    if( e != null ) onError(e, emsg);
    else
      handle = Stream._hx_uv_tcp_connect(ip, server_port, on_connected, this);
  }
}

class Server extends Stream {
  public function listen() {}
  public dynamic function onNewClient(cl:StreamInterface):Void {}
}

private class StreamServer<ClientClass:ClientAdapter> extends Server {

  function on_new_client(?h:Dynamic, ?errcode:Int, ?errmsg:String) {
    if(errcode != null) this.onError(errcode, errmsg);
    else {
      var tc:ClientClass = cast(new ClientAdapter(h));
      this.onNewClient(tc);
    }
  }
}

class TCPServer extends StreamServer<TCPClient> {

  var addr:String;
  var port:Int;

  public function new( port:Int, host:String = "0.0.0.0" ) {
    super();

    this.addr = host;
    this.port = port;
  }

  override public function listen() {
    handle = Stream._hx_uv_tcp_listen(
        Lib.haxeToNeko(addr), port, on_new_client, this);
  }
}

class PipeClient extends ClientAdapter {

  private var pipe_path:String;

  public function new( path:String ) {
    super();
    this.pipe_path = path;
  }

  override public function peer():{ port:Int, host:sys.net.Host } {
    return { port: null, host: null };
  }

  override public function host():{ port:Int, host:sys.net.Host } {
    return { port: null, host: null };
  }

  public function connect() {
    if( this.connected ) return;
    var data = Lib.haxeToNeko(this.pipe_path);
    this.handle = Stream._hx_uv_pipe_connect(data, on_connected, this);
  }
}

class PipeServer extends StreamServer<PipeClient> {

  var pipe_path:String;

  public function new( pipe:String ) {
    super();
    this.pipe_path = pipe;
  }

  override public function listen() {
    var data = Lib.haxeToNeko(this.pipe_path);
    this.handle = Stream._hx_uv_pipe_listen(data, on_new_client, this);
  }

}

// TODO: Implement UDP
class UDP extends Stream {}

typedef StreamInterface = StreamIOOperations;
typedef IStream = StreamIOOperations;