
package hxuv;

#if !neko
  #error "only Neko are allowed"
#end

import neko.Lib;
import neko.vm.Tls;

class Handle {
  private var handle:Dynamic;
}

class IOLoop {

  private static var inst:Tls<IOLoop>;

  var loop:Dynamic;
  var running:Bool = false;
  var cpus:Array<Dynamic>;

  public static var LIBNAME(default,never):String ="ndll/" + Sys.systemName() + "/nuv";

  private function new() {
    this.loop = IOLoop._hx_uv_init();
    this.cpus = Lib.nekoToHaxe(_hx_uv_get_cpu_info());
    if(inst == null ) inst = new Tls();
    inst.value = this;
  }

  public static function getNetworkInterfaces():Array<{name:String, type:String, ip:String, internal:Bool}> {
    return Lib.nekoToHaxe(_hx_nuv_get_network_interfaces());
  }

  public static function getInstance() {
    return IOLoop.inst == null || IOLoop.inst.value == null  ? new IOLoop() : IOLoop.inst.value;
  }

  public function run():Void {
    if( this.running ) return;

    this.running = true;
    IOLoop._hx_uv_run();
    this.running = false;
  }

  public  function run_once():Void {
    if( this.running ) return;

    this.running = true;
    IOLoop._hx_uv_run_once();
    this.running = false;
  }

  public static function start() {
    IOLoop.getInstance().run();
  }

  public static function getCPUCount():Int {
    return IOLoop.getInstance().cpus.length;
  }

  static var _hx_uv_init = Lib.load(IOLoop.LIBNAME, "hx_nuv_loop_init", 0);
  static var _hx_uv_shutdown = Lib.load(IOLoop.LIBNAME, "hx_nuv_loop_shutdown", 0);
  static var _hx_uv_run = Lib.load(IOLoop.LIBNAME, "hx_nuv_loop_run", 0);
  static var _hx_uv_run_once = Lib.load(IOLoop.LIBNAME, "hx_nuv_loop_run_once", 0);
  static var _hx_uv_get_cpu_info = Lib.load(IOLoop.LIBNAME, "hx_nuv_get_cpu_info", 0);
  static var _hx_nuv_get_network_interfaces = Lib.load(IOLoop.LIBNAME, "hx_nuv_get_network_interfaces", 0);
}
