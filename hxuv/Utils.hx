

package hxuv;

import hxuv.Loop.IOLoop;

#if ( !cpp )
  import neko.vm.Thread;
  import neko.vm.Deque;
  import neko.Lib;
#end


private class Utils {

  private var loop:IOLoop;
  public dynamic function onerror(ecode:Int, emsg:String):Void {}

  public function new() {
    loop = IOLoop.getInstance();
  }


  private static var _hx_nuv_async_exec = Lib.load(IOLoop.LIBNAME, "hx_nuv_async_exec", 1);
  private static var _hx_nuv_async_init = Lib.load(IOLoop.LIBNAME, "hx_nuv_async_init", 2);

  private static var _hx_nuv_timer_init = Lib.load(IOLoop.LIBNAME, "hx_nuv_timer_init", 1);
  private static var _hx_nuv_timer_start = Lib.load(IOLoop.LIBNAME, "hx_nuv_timer_start", 3);
  private static var _hx_nuv_timer_stop = Lib.load(IOLoop.LIBNAME, "hx_nuv_timer_stop", 1);
  private static var _hx_nuv_timer_again = Lib.load(IOLoop.LIBNAME, "hx_nuv_timer_again", 1);
}


private typedef AWJob = {
  complete_cb: Dynamic -> Void,
  work: Dynamic -> Dynamic,
  param: Dynamic,
  data: Dynamic
}

private class AsyncWorkerPool extends Utils  {

  // Implement NEKO Based Thread Worker
  // Pool With libuv Async notification

  private static var pools:Array<Thread>;
  private static var queue:Deque<AWJob>;
  private static var handle:Dynamic;

  private static var is_started:Bool;

  private static function bootstrap() {

    if( is_started ) return;

    is_started = true;

    var main_t = Thread.current();
    var q  = AsyncWorkerPool.queue = new Deque<AWJob>();
    var h = AsyncWorkerPool.handle = Utils._hx_nuv_async_init(on_async_notify, null);

    AsyncWorkerPool.pools = new Array<Thread>();

    var thread_fn = function() {
            do {

              var job:AWJob = q.pop(true);
              if( job == null ) break;

              try {
                if( job.work != null )
                  job.data = job.work( job.param );

                main_t.sendMessage(job);
                Utils._hx_nuv_async_exec(h);
              } catch( e:Dynamic ) { trace(e); }
            } while( true );
          };

    for(i in 0...IOLoop.getCPUCount())
      AsyncWorkerPool.pools.insert(i, Thread.create(thread_fn));
  }

  private static function on_async_notify() {

    do {
      var job:AWJob = cast( Thread.readMessage(false) );
      if( job == null ) break;

      try {
        if( job.complete_cb != null ) job.complete_cb( job.data );
      } catch( e:Dynamic ) { trace(e); }

    } while( true );
  }

  public static function queue_job( job:AWJob ) {
    if( !is_started ) bootstrap();

    AsyncWorkerPool.queue.push( job );
  }

}

class AsyncWorker extends Utils {

  private var handle:Dynamic;

  dynamic public function onWorkComplete(r:Dynamic):Void {}
  dynamic public function work( p:Dynamic ):Dynamic {}

  public function run(p:Dynamic) {
    var j:AWJob = {
      param : p,
      complete_cb : this.onWorkComplete,
      work : this.work,
      data: null
    }

    AsyncWorkerPool.queue_job(j);
  }

}

class Timer extends Utils {

  private var timeout:Int;
  private var hdl:Dynamic;
  private var is_started:Bool;

  public dynamic function onTimeout(t:Timer):Void {}

  public function new( time_ms:Int ) {
    super();
    this.timeout = time_ms;
    this.hdl = Utils._hx_nuv_timer_init( callback(this.handle) );
  }

  public function start() {
    if( this.is_started == true ) return;
    Utils._hx_nuv_timer_start( this.hdl, this.timeout, 0 );
    this.is_started = true;
  }

  public function stop() {
    if( this.is_started == false ) return;
    Utils._hx_nuv_timer_stop( this.hdl );
    this.is_started = false;
  }

  private function handle( ) {
    this.stop();
    this.onTimeout(this);
  }
}

class PeriodicTimer extends Utils {

  private var timeout:Int;
  private var hdl:Dynamic;
  private var is_started:Bool;

  public dynamic function onTimeout(pt:PeriodicTimer):Void {}

  public function new( time_ms:Int ) {
    super();
    timeout = time_ms;
    hdl = Utils._hx_nuv_timer_init( callback(this.handle) );
  }

  public function start() {
    if( is_started == true ) return;
    Utils._hx_nuv_timer_start( hdl, timeout, timeout );
    is_started = true;
  }

  public function stop() {
    if( is_started == false ) return;
    Utils._hx_nuv_timer_stop( hdl );
    is_started = false;
  }

  private function handle( ) {
    this.onTimeout(this);
  }
}

private typedef TQ = {
  func:Void -> Void,
  timer:Dynamic,
  idx:Int
}

class TimerQueue extends Utils {

  private var timeout:Int;
  private var queues:IntHash<TQ>;
  private var _idx:Int;

  public function new( time_ms:Int ) {

    super();

    _idx = 1;
    timeout = time_ms;
    queues = new IntHash();
  }

  public function add( f:Void -> Void ):Int {

    var t:TQ = {func: f, timer:null, idx: _idx};

    _idx = t.idx + 1;
    queues.set(t.idx, t);
    t.timer = Utils._hx_nuv_timer_init( callback(this.handle, t.idx) );

    Utils._hx_nuv_timer_start( t.timer, this.timeout, this.timeout );

    return t.idx;
  }

  public function stop( idx:Int ) {
    var t = queues.get(idx);
    if( t != null && t.idx == idx )
      queues.remove(t.idx) && Utils._hx_nuv_timer_stop( t.timer );
  }

  private function handle( idx:Int ) {
    var t = queues.get(idx);
    if( t != null && t.idx == idx ) {
      t.func();
      this.stop( t.idx );
    }
  }

}
