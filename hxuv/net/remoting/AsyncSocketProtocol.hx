
// implement haxe.remoting.SocketProtocol for hxnuv

package hxuv.net.remoting;

import hxuv.Stream.StreamInterface;
import haxe.remoting.Context;

/**
  The haXe Remoting Socket Protocol is composed of serialized string exchanges.
  Each string is prefixed with a 2-chars header encoding the string size (up to 4KB)
  and postfixed with the \0 message delimiting char.
  A request string is composed of the following serialized values :
      - the boolean true for a request
      - an array of strings representing the object+method path
      - an array of parameters
  A response string is composed of the following serialized values :
      - the boolean false for a response
      - a serialized value representing the result
  Exceptions are serialized with [serializeException] so they will be thrown immediatly
  when they are unserialized.
**/

class AsyncSocketProtocol {

    var asyncAnswer:Bool = false;
	public var socket : StreamInterface;
	public var context : Context;

	public function new( sock, ctx ) {
		this.socket = sock;
		this.context = ctx;

        this.socket.onWriteComplete = callback(onSocketDataSent);
        this.socket.onError = callback(onSocketError);
	}

    function onSocketDataSent() {
      this.onSendMessageDone();
    }

    function onSocketError(e:Int, em:String) {
      this.onError(e,em);
    }

	function decodeChar(c) : Null<Int> {
		// A...Z
		if( c >= 65 && c <= 90 )
			return c - 65;
		// a...z
		if( c >= 97 && c <= 122 )
			return c - 97 + 26;
		// 0...9
		if( c >= 48 && c <= 57 )
			return c - 48 + 52;
		// +
		if( c == 43 )
			return 62;
		// /
		if( c == 47 )
			return 63;
		return null;
	}

	function encodeChar(c) : Null<Int> {
		if( c < 0 )
			return null;
		// A...Z
		if( c < 26 )
			return c + 65;
		// a...z
		if( c < 52 )
			return (c - 26) + 97;
		// 0...9
		if( c < 62 )
			return (c - 52) + 48;
		// +
		if( c == 62 )
			return 43;
		// /
		if( c == 63 )
			return 47;
		return null;
	}

    /*
      Allows user to hook and process answer if necessary
      User Case: Using process based callback you can universally send back
      answers without hooking everywhere
    */
    public function useAsyncAnswer(yes:Bool) {
      this.asyncAnswer = yes;
    }

    dynamic public function sendAsyncAnswer(proto: AsyncSocketProtocol, answer : Dynamic, ?isException : Bool ) { }

	public function messageLength( c1 : Int, c2 : Int ) : Null<Int> {
		var e1 = decodeChar(c1);
		var e2 = decodeChar(c2);
		if( e1 == null || e2 == null )
			return null;
		return (e1 << 6) | e2;
	}

	public function encodeMessageLength( len : Int ) {
		var c1 = encodeChar(len>>6);
		if( c1 == null )
			throw "Message is too big";
		var c2 = encodeChar(len&63);
		return { c1 : c1, c2 : c2 };
	}

	public function sendRequest( path : Array<String>, params : Array<Dynamic> ) {
		var s = new haxe.Serializer();
		s.serialize(true);
		s.serialize(path);
		s.serialize(params);
		sendMessage(s.toString());
	}

	public function sendAnswer( answer : Dynamic, ?isException : Bool ) {
		var s = new haxe.Serializer();
		s.serialize(false);
		if( isException )
			s.serializeException(answer);
		else
			s.serialize(answer);
		sendMessage(s.toString());
	}

	public function sendMessage( msg : String ) {
		var e = encodeMessageLength(msg.length + 3);
		var o = new haxe.io.BytesOutput();
        o.prepare(msg.length + 3);
		o.writeByte(e.c1);
		o.writeByte(e.c2);
		o.writeString(msg);
		o.writeByte(0);
        o.close();

        socket.send(neko.Lib.stringReference(o.getBytes()));

	}

	public dynamic function decodeData( data : String ) {
		return data;
	}

    public dynamic function onSendMessageDone():Void {

    }


    public dynamic function onError(e:Int, emsg:String):Void {

    }

	public function isRequest( data : String ) {
		return switch( haxe.Unserializer.run(data) ) {
		case true: true;
		case false: false;
		default: throw "Invalid data";
		}
	}

	public function processRequest( data : String, ?onError : Array<String> -> Array<Dynamic> -> Dynamic -> Void ) {
		var s = new haxe.Unserializer(data);
		var result : Dynamic;
		var isException = false;
		if( s.unserialize() != true )
			throw "Not a request";
		var path : Array<String> = s.unserialize();
		var args : Array<Dynamic> = s.unserialize();
		try {
			if( context == null ) throw "No context is shared";
			result = context.call(path,args);
		} catch( e : Dynamic ) {
			result = e;
			isException = true;
		}
		// send back result/exception over network
        if( !this.asyncAnswer )
          sendAnswer(result,isException);
        else sendAsyncAnswer(this,result,isException);

		// send the error event
		if( isException && onError != null )
			onError(path,args,result);
	}

	public function processAnswer( data : String ) : Dynamic {
		var s = new haxe.Unserializer(data);
		if(  s.unserialize() != false ) throw "Not an answer";
		return s.unserialize();
	}


    function onSocketData(oldfunc:Dynamic, out:haxe.io.BytesOutput, fn: Dynamic -> Void, msg:String ) {

      out.writeString(msg);

      var data:Dynamic;
      var b = out.getBytes();

      var i = new haxe.io.BytesInput( b );
      var c1 = i.readByte();
      var c2 = i.readByte();
      var len = messageLength(c1,c2);

      do {

		if( len == null ) {
          data = haxe.io.Error.Custom("Invalid message");
          break;
        }

        if( len > b.length ) return; // go read more data

        data = i.readString(len - 3);
		if( i.readByte() != 0 ) {
          data = haxe.io.Error.Custom("Invalid message");
          break;
        }

        data = decodeData( data );

      } while(false);

      socket.onDataReady = oldfunc;

      out.close();
      b = null; // free all this data

      fn( data );
    }

	public function readMessage( fn: Dynamic -> Void) {
        if( socket.onDataReady != null ) return;

        var out = new haxe.io.BytesOutput();
        var oldfunc = socket.onDataReady;
        socket.onDataReady = callback(onSocketData, oldfunc, out, fn);
        socket.read();
	}

}
