
/* implements haxe.remoting.SocketConnection for hxuv */
package hxuv.net.remoting;

import hxuv.Stream.StreamInterface;
import hxuv.net.remoting.AsyncSocketProtocol;

import haxe.remoting.AsyncConnection;
import haxe.remoting.Context;

typedef AsyncSocket = StreamInterface;

class AsyncSocketConnection implements AsyncConnection, implements Dynamic<AsyncConnection> {

	var __path : Array<String>;
	var __data : {
		protocol : AsyncSocketProtocol,
		results : List<{ onResult : Dynamic -> Void, onError : Dynamic -> Void }>,
		log : Array<String> -> Array<Dynamic> -> Dynamic -> Void,
		error : Dynamic -> Void,
	};

	function new(data,path) {
		__data = data;
		__path = path;

        __data.protocol.onError = callback(onProtocolError);
	}

    function onProtocolError(e:Int, emsg:String) {

      var eo:Dynamic;
      if( e == 1 ) eo = new haxe.io.Eof();
      else eo = haxe.io.Error.Custom({"no":e, "info":emsg});

      __data.error(eo);
    }

	public function resolve(name) : AsyncConnection {
		var s = new AsyncSocketConnection(__data,__path.copy());
		s.__path.push(name);
		return s;
	}

	public function call( params : Array<Dynamic>, ?onResult : Dynamic -> Void ) {
		try {
			__data.protocol.sendRequest(__path, params);
			__data.results.add({ onResult : onResult, onError : __data.error });
		} catch( e : Dynamic ) {
			__data.error(e);
		}
	}

	public function setErrorHandler(h) {
		__data.error = h;
	}

	public function setErrorLogger(h) {
		__data.log = h;
	}

	public function setProtocol( p : AsyncSocketProtocol ) {
		__data.protocol = p;
	}

	public function getProtocol() : AsyncSocketProtocol {
		return __data.protocol;
	}

	public function close() {
		try getProtocol().socket.close() catch( e : Dynamic ) { };
	}

	public function processMessage( data : String ) {
		var request;
		var proto = getProtocol();
		data = proto.decodeData(data);
		try {
			request = proto.isRequest(data);
		} catch( e : Dynamic ) {
			var msg = Std.string(e) + " (in "+StringTools.urlEncode(data)+")";
			__data.error(msg); // protocol error
			return;
		}
		// request
		if( request ) {
			try proto.processRequest(data,__data.log) catch( e : Dynamic ) __data.error(e);
			return;
		}
		// answer
		var f = __data.results.pop();
		if( f == null ) {
			__data.error("No response excepted ("+data+")");
			return;
		}
		var ret;
		try {
			ret = proto.processAnswer(data);
		} catch( e : Dynamic ) {
			f.onError(e);
			return;
		}
		if( f.onResult != null ) f.onResult(ret);
	}

	function defaultLog(path,args,e) {
		// exception inside the called method
		var astr, estr;
		try astr = args.join(",") catch( e : Dynamic ) astr = "???";
		try estr = Std.string(e) catch( e : Dynamic ) estr = "???";
		var header = "Error in call to "+path.join(".")+"("+astr+") : ";
		__data.error(header + estr);
	}

	public static function create( s : AsyncSocket, ?ctx : Context ) {
		var data = {
			protocol : new AsyncSocketProtocol(s, ctx),
			results : new List(),
			error : function(e) throw e,
			log : null
		};
		var sc = new AsyncSocketConnection(data,[]);
		data.log = sc.defaultLog;
		return sc;
	}

}
