
package hxuv.net.remoting;

import hxuv.Stream.Server;
import hxuv.Stream.TCPServer;
import hxuv.Stream.PipeServer;
import hxuv.Stream.StreamInterface;

import hxuv.net.remoting.AsyncSocketProtocol;
import hxuv.net.remoting.AsyncSocketConnection;

import hxuv.Utils.TimerQueue;

private typedef RClientInfo = {
  var client : AsyncSocketConnection;
  var sock : AsyncSocket;
  var buf : haxe.io.Bytes;
  var bufpos : Int;
}

class AsyncRemotingServer {

	var sock : Server;
	var timer : TimerQueue;
    var port : Int;

    var clients:haxe.FastList<RClientInfo>;
    var domains:Null<Array<String>>;

	public var listen : Int;
	public var connectLag : Float;
	public var errorOutput : haxe.io.Output;
	public var initialBufferSize : Int;
	public var maxBufferSize : Int;
	public var messageHeaderSize : Int;
	public var updateTime : Float;


	public function new(?domains:Array<String>) {
		listen = 10;
		connectLag = 0.5;
		errorOutput = Sys.stderr();
		initialBufferSize = (1 << 10);
		maxBufferSize = (1 << 16);
		updateTime = 1;
        messageHeaderSize = 2;

        this.clients = new haxe.FastList();
		this.domains = domains;
	}

    function onClientClosed(s:AsyncSocket) {
      this.clients.remove(s.custom);
    }

	function doClientDisconnected(s,c) {
		try s.close() catch( e : Dynamic ) {};
		clientDisconnected(c);
	}

	function logError( e : Dynamic ) {
      onError(e, haxe.Stack.exceptionStack());
	}

    function onClientDataReady(sock : AsyncSocket, data:String) {

      var c:RClientInfo = sock.custom;

      var inbuf = haxe.io.Bytes.ofString(data);

      var available = c.buf.length - c.bufpos;
      if( available == 0 || available < inbuf.length) {
          var newsize = c.bufpos + inbuf.length;
          if( newsize > maxBufferSize ) {
              newsize = maxBufferSize;
              if( c.buf.length == maxBufferSize ) {
                  this.logError("Max buffer size reached");
                  return;
                }
          }
          var newbuf = haxe.io.Bytes.alloc(newsize);
          newbuf.blit(0,c.buf,0,c.bufpos);
          c.buf = newbuf;
          available = newsize - c.bufpos;
      }

      c.buf.blit(c.bufpos, inbuf, 0, inbuf.length);
      var pos = 0;
      var len = c.bufpos + inbuf.length;
      while( len >= messageHeaderSize ) {
          var m ;
          try { m = readClientMessage(c.client,c.buf,pos,len); }
          catch(e:Dynamic) {this.logError(e); doClientDisconnected(sock, c.client); return; }

          if( m == null ) break;
          pos += m.bytes;
          len -= m.bytes;
          clientMessage(c.client, m.msg);
      }
      if( pos > 0 )
        c.buf.blit(0,c.buf,pos,len);

      c.bufpos = len;
    }

	function addClient( sock : AsyncSocket ) {
      var infos : RClientInfo = {
          client : clientConnected(sock),
          sock : sock,
          buf : haxe.io.Bytes.alloc(initialBufferSize),
          bufpos : 0,
      };

      clients.add(infos);

      sock.onDataReady = callback(onClientDataReady, sock);
      sock.onClosed = callback(onClientClosed, sock);

      sock.custom = infos;
      sock.read(); // Start Receving Remoting Data
	}

	function init() {
      sock.listen();
      sock.onNewClient = callback(addClient);
	}


    public function runPipe(unix_path:String) {
      sock = new PipeServer(unix_path);
      init();
    }

	public function run( port:Int, host:String = "0.0.0.0" ) {
		sock = new TCPServer(port, host);
        this.port = port;
		init();
	}

	public function sendData( s : AsyncSocket, data : String ) {
		try {
			s.send(data);
		} catch( e : Dynamic ) {
			stopClient(s);
		}
	}

	public function stopClient( s : AsyncSocket ) {
		var infos : RClientInfo = s.custom;
        clients.remove(infos);
        s.custom = null;
        s.close();
	}

	// --- CUSTOMIZABLE API ---

	public dynamic function onError( e : Dynamic, stack ) {
		var estr = try Std.string(e) catch( e2 : Dynamic ) "???" + try "["+Std.string(e2)+"]" catch( e : Dynamic ) "";
		errorOutput.writeString( estr + "\n" + haxe.Stack.toString(stack) );
		errorOutput.flush();
	}


	public dynamic function clientDisconnected( c : AsyncSocketConnection ) {}
	public dynamic function update() { }


    public dynamic function initClientApi( cnx : AsyncSocketConnection, ctx : haxe.remoting.Context ) {
		throw "Not implemented";
	}

	public dynamic function onXml( cnx : AsyncSocketConnection, data : String ) {
		throw "Unhandled XML data '"+data+"'";
	}

	public dynamic function makePolicyFile() {
		var str = "<cross-domain-policy>";
		for( d in domains )
			str += '<allow-access-from domain="'+d+'" to-ports="'+port+'"/>';
		str += "</cross-domain-policy>";
		return str;
	}


	public function clientConnected( s : AsyncSocket ) {
		var ctx = new haxe.remoting.Context();
		var cnx = AsyncSocketConnection.create(s,ctx);
		var me = this;
		cnx.setErrorHandler(function(e) {
			if( !Std.is(e,haxe.io.Eof) && !Std.is(e,haxe.io.Error) )
				me.logError(e);

			me.stopClient(s);
		});
		initClientApi(cnx,ctx);
		return cnx;
	}

	function readClientMessage( cnx : AsyncSocketConnection, buf : haxe.io.Bytes, pos : Int, len : Int ) {
		var msgLen = cnx.getProtocol().messageLength(buf.get(pos),buf.get(pos+1));
		if( msgLen == null ) {
			if( buf.get(pos) != 60 )
				throw "Invalid remoting message '"+buf.readString(pos,len)+"'";
			var p = pos;
			while( p < len ) {
				if( buf.get(p) == 0 )
					break;
				p++;
			}
			if( p == len )
				return null;
			p -= pos;
			return {
				msg : buf.readString(pos,p),
				bytes : p + 1,
			};
		}
		if( len < msgLen )
			return null;
		if( buf.get(pos + msgLen-1) != 0 )
			throw "Truncated message";
		return {
			msg : buf.readString(pos+2,msgLen-3),
			bytes : msgLen,
		};
	}

	public function clientMessage( cnx : AsyncSocketConnection, msg : String ) {
		try {
			if( msg.charCodeAt(0) == 60 ) {
				if( domains != null && msg == "<policy-file-request/>" )
					cnx.getProtocol().socket.send(makePolicyFile()+"\x00");
				else
					onXml(cnx,msg);
			} else
				cnx.processMessage(msg);
		} catch( e : Dynamic ) {
			if( !Std.is(e,haxe.io.Eof) && !Std.is(e,haxe.io.Error) )
				logError(e);
			stopClient(cnx.getProtocol().socket);
		}
	}

}
