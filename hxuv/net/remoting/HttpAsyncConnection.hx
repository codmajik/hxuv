
// implements haxe.remoting.HttpAsyncConnection for hxuv
package hxuv.net.remoting;

import hxuv.net.Http;

class HttpAsyncConnection implements Dynamic<HttpAsyncConnection> {

	var __data : { url : String, error : Dynamic -> Void };
	var __path : Array<String>;

    var http:AsyncHttp;

	function new(data,path) {
		__data = data;
		__path = path;
	}

	public function resolve( name ) {
        var c = Type.createInstance(Type.getClass(this), [__data, __path.copy()]);
		c.__path.push(name);
		return c;
	}

	public function setErrorHandler(h) {
		__data.error = h;
	}

    dynamic public function calla( params : Array<Dynamic> ):Dynamic {}

	public function call( params : Array<Dynamic>, ?onResult : Dynamic -> Void ) {

		var h = this.http = new AsyncHttp(__data.url);

		var s = new haxe.Serializer();
		s.serialize(__path);
		s.serialize(params);
		h.setHeader("X-Haxe-Remoting","1");
		h.setParameter("__x",s.toString());
		var error = __data.error;
		var data_cb = function( _this:Dynamic, c:AsyncHttp, response : String ) {
			var ok = true;
			var ret;
			try {
				if( response.substr(0,3) != "hxr" )
                  throw "Invalid response : '"+response+"' -> " + c.url ;

				var s = new haxe.Unserializer(response.substr(3));
				ret = s.unserialize();
			} catch( err : Dynamic ) {
				ret = null;
				ok = false;
				error(err);
			}
			if( ok && onResult != null ) onResult(ret);
		};

        // keep a reference to asynchttp -- libgc keeps losing reference to it
        h.onData = callback(data_cb,this, h);
		h.onError = error;
		h.request(true);
	}

	public static function urlConnect( url : String ) {
		return new HttpAsyncConnection({ url : url, error : function(e) throw e },[]);
	}

}
