
// a port of haxe.Http for hxuv
package hxuv.net;

import hxuv.Stream.TCPClient;


private typedef Response = {
    out:haxe.io.BytesOutput,
    headers:Array<String>,
    status:Int,
    size:Null<Int>,
    chunked:Bool,
    tmpBuf:haxe.io.BytesBuffer

}

class AsyncHttp {

	public var url : String;

	public var cnxTimeout : Float;
	public var responseHeaders : Hash<String>;
	var chunk_size : Null<Int>;
	var chunk_buf : haxe.io.Bytes;
	var file : { param : String, filename : String, io : haxe.io.Input, size : Int };

	var postData : String;
	var headers : Hash<String>;
	var params : Hash<String>;
    var sock:TCPClient;


    var is_busy:Bool;
    var url_regexp:EReg;

	public static var PROXY : { host : String, port : Int, auth : { user : String, pass : String } } = null;

	/**
	 * @param	url
	 */
	public function new( url : String ) {
      this.url = url;
      headers = new Hash();
      params = new Hash();
      cnxTimeout = 10; //TODO: Implement Timeout

      is_busy = false;

      url_regexp = ~/^(https?:\/\/)?([a-zA-Z\.0-9-]+)(:[0-9]+)?(.*)$/;
      if( !url_regexp.match(url) ) throw "Invalid URL";
      if( (url_regexp.matched(1) == "https://") ) throw "Https is not supported";

      var host = url_regexp.matched(2);
      var port = 80;

      if( PROXY == null ) {
        var portString = url_regexp.matched(3);
        if ( portString != null && portString != "" )
          port = Std.parseInt(portString.substr(1, portString.length - 1));
      } else {
        host = PROXY.host;
        port = PROXY.port;
      }

      sock = new TCPClient(host, port);
      sock.onWriteComplete = callback(on_socket_write_done);
      sock.onError = callback(on_socket_error);
	}

	public function setHeader( header : String, value : String ) {
		headers.set(header,value);
	}

	public function setParameter( param : String, value : String ) {
		params.set(param,value);
	}

	public function setPostData( data : String ) {
		postData = data;
	}

	public function request( post : Bool ) : Void {
        if( is_busy ) return;

        var out:Response = {
          out : new haxe.io.BytesOutput(),
          headers : null,
          status: 0,
          size: null,
          chunked: false,
          tmpBuf: new haxe.io.BytesBuffer()
        }

        sock.onConnected = callback(on_socket_connected, post);
        sock.onDataReady = callback(readHttpResponse, out);
        sock.connect();

        is_busy = true;
	}

	public function fileTransfert( argname : String, filename : String, file : haxe.io.Input, size : Int ) {
		this.file = { param : argname, filename : filename, io : file, size : size };
	}

	function customRequest( post : Bool, ?method : String  ) {

        var host = url_regexp.matched(2);
		var port_s = url_regexp.matched(3);
		var request = url_regexp.matched(4);
		if( request == "" ) request = "/";

        var secure = (url_regexp.matched(1) == "https://");

        if( secure ) throw "Https is not supported";

		var port = if ( port_s == null || port_s == "" ) secure ? 443 : 80 else Std.parseInt(port_s.substr(1, port_s.length - 1));
		var data;

		var multipart = (file != null);
		var boundary = null;
		var uri = null;
		if( multipart ) {
			post = true;
			boundary = Std.string(Std.random(1000))+Std.string(Std.random(1000))+Std.string(Std.random(1000))+Std.string(Std.random(1000));
			while( boundary.length < 38 )
				boundary = "-" + boundary;
			var b = new StringBuf();
			for( p in params.keys() ) {
				b.add("--");
				b.add(boundary);
				b.add("\r\n");
				b.add('Content-Disposition: form-data; name="');
				b.add(p);
				b.add('"');
				b.add("\r\n");
				b.add("\r\n");
				b.add(params.get(p));
				b.add("\r\n");
			}
			b.add("--");
			b.add(boundary);
			b.add("\r\n");
			b.add('Content-Disposition: form-data; name="');
			b.add(file.param);
			b.add('"; filename="');
			b.add(file.filename);
			b.add('"');
			b.add("\r\n");
			b.add("Content-Type: "+"application/octet-stream"+"\r\n"+"\r\n");
			uri = b.toString();
		} else {
			for( p in params.keys() ) {
				if( uri == null )
					uri = "";
				else
					uri += "&";
				uri += StringTools.urlEncode(p)+"="+StringTools.urlEncode(params.get(p));
			}
		}

		var b = new StringBuf();
		if( method != null ) {
			b.add(method);
			b.add(" ");
		} else if( post )
			b.add("POST ");
		else
			b.add("GET ");

		if( AsyncHttp.PROXY != null ) {
			b.add("http://");
			b.add(host);
			if( port != 80 ) {
				b.add(":");
				b.add(port);
			}
		}
		b.add(request);

		if( !post && uri != null ) {
			if( request.indexOf("?",0) >= 0 )
				b.add("&");
			else
				b.add("?");
			b.add(uri);
		}
		b.add(" HTTP/1.1\r\nHost: "+host+"\r\n");
		if( postData != null )
			b.add("Content-Length: "+postData.length+"\r\n");
		else if( post && uri != null ) {
			if( multipart || headers.get("Content-Type") == null ) {
				b.add("Content-Type: ");
				if( multipart ) {
					b.add("multipart/form-data");
					b.add("; boundary=");
					b.add(boundary);
				} else
					b.add("application/x-www-form-urlencoded");
				b.add("\r\n");
			}
			if( multipart )
				b.add("Content-Length: "+(uri.length+file.size+boundary.length+6)+"\r\n");
			else
				b.add("Content-Length: "+uri.length+"\r\n");
		}
		for( h in headers.keys() ) {
			b.add(h);
			b.add(": ");
			b.add(headers.get(h));
			b.add("\r\n");
		}
		b.add("\r\n");
		if( postData != null) b.add(postData);
		else if( post && uri != null ) b.add(uri);

        sock.send(b.toString());
        if( multipart ) { // TODO: Use libuv based sendfile
          var bufsize = 4096;
          var buf = haxe.io.Bytes.alloc(bufsize);
          while( file.size > 0 ) {
            var size = if( file.size > bufsize ) bufsize else file.size;
            var len = 0;
            try { len = file.io.readBytes(buf,0,size);
            } catch( e : haxe.io.Eof ) break;
            sock.send(buf.readString(0,len));
            file.size -= len;
          }
          sock.send("\r\n--" +boundary + "--");
        }
	}

	function readHttpResponse( api : Response, data:String ):Void {

      var inp = new haxe.io.StringInput(data);

      // READ the HTTP header (until \r\n\r\n)
      var b = new haxe.io.BytesBuffer();
      var k = 4;
      var s = haxe.io.Bytes.alloc(4);
      if(api.headers == null) {
        while( true ) {
            try {
              var p = inp.readBytes(s,0,k);

              while( p != k )
                p += inp.readBytes(s,p,k - p);
              b.addBytes(s,0,k);

            } catch(e:haxe.io.Eof) {
              api.tmpBuf.add(b.getBytes());
              return; // more data to read
            }

            switch( k ) {
            case 1:
                var c = s.get(0);
                if( c == 10 )
                    break;
                if( c == 13 )
                    k = 3;
                else
                    k = 4;
            case 2:
                var c = s.get(1);
                if( c == 10 ) {
                    if( s.get(0) == 13 )
                        break;
                    k = 4;
                } else if( c == 13 )
                    k = 3;
                else
                    k = 4;
            case 3:
                var c = s.get(2);
                if( c == 10 ) {
                    if( s.get(1) != 13 )
                        k = 4;
                    else if( s.get(0) != 10 )
                        k = 2;
                    else
                        break;
                } else if( c == 13 ) {
                    if( s.get(1) != 10 || s.get(0) != 13 )
                        k = 1;
                    else
                        k = 3;
                } else
                    k = 4;
            case 4:
                var c = s.get(3);
                if( c == 10 ) {
                    if( s.get(2) != 13 )
                        continue;
                    else if( s.get(1) != 10 || s.get(0) != 13 )
                        k = 2;
                    else
                        break;
                } else if( c == 13 ) {
                    if( s.get(2) != 10 || s.get(1) != 13 )
                        k = 3;
                    else
                        k = 1;
                }
            }
        }

        api.tmpBuf.add(b.getBytes());

        b = api.tmpBuf;

        #if neko
        api.headers = neko.Lib.stringReference(b.getBytes()).split("\r\n");
        #else
        api.headers = b.getBytes().toString().split("\r\n");
        #end
      }

      var size = api.size;
      var chunked = api.chunked;

      if( size == null  && !chunked ) {
        var headers = api.headers;

        var response = headers.shift();
        var rp = response.split(" ");
        var status = api.status = Std.parseInt(rp[1]);
        if( status == 0 || status == null )
            throw "Response status error";

        // remove the two lasts \r\n\r\n
        headers.pop();
        headers.pop();
        responseHeaders = new Hash();
        for( hline in headers ) {
            var a = hline.split(": ");
            var hname = a.shift();
            var hval = if( a.length == 1 ) a[0] else a.join(": ");
            responseHeaders.set(hname, hval);
            switch(hname.toLowerCase())
            {
                case "content-length":
                    size = api.size = Std.parseInt(hval);
                case "transfer-encoding":
                    chunked = api.chunked = (hval.toLowerCase() == "chunked");
            }
        }

        onStatus(status);

        chunk_size = null;
        chunk_buf = null;
      }

      var chunk_re = ~/^([0-9A-Fa-f]+)[ ]*\r\n/m;


      if( size != null ) api.out.prepare(size);

      try {
        var buf = inp.readAll(size);
        if( chunked ) {
          if( readChunk(chunk_re,api.out,buf, buf.length) ) return;
        }
        else api.out.writeBytes(buf, 0, buf.length);

        if( size != null ) {
          size -= buf.length;
          api.size = size;

          if(size > 0) return;
        }
      }
      catch( e : haxe.io.Eof ) return;

      sock.pauseRead();
      sock.close();
      sock = null;
      api.out.close();

      if( chunked && (chunk_size != null || chunk_buf != null) )
          return onErrorFull(0, "Invalid chunk");

      if( api.status < 200 || api.status >= 400 )
          return onErrorFull(0, "Http Error #" + api.status);


      api.tmpBuf = null;


      try {

        #if neko
          this.onData(neko.Lib.stringReference(api.out.getBytes()));
        #else
          this.onData(api.out.getBytes().toString());
        #end
      } catch(e:Dynamic) {
        trace(e);
      }

      api.out = null;


      return;

	}

	function readChunk(chunk_re : EReg, api : haxe.io.Output, buf : haxe.io.Bytes, len ) {

		if( chunk_size == null ) {
			if( chunk_buf != null ) {
				var b = new haxe.io.BytesBuffer();
				b.add(chunk_buf);
				b.addBytes(buf,0,len);
				buf = b.getBytes();
				len += chunk_buf.length;
				chunk_buf = null;
			}


			#if neko
              var match_buf = neko.Lib.stringReference(buf);
			#else
              var match_buf = buf.toString();
			#end

            if( chunk_re.match(match_buf) ) {
				var p = chunk_re.matchedPos();
				if( p.len <= len ) {
					var cstr = chunk_re.matched(1);
					chunk_size = Std.parseInt("0x"+cstr);
					if( cstr == "0" ) {
						chunk_size = null;
						chunk_buf = null;
						return false;
					}
					len -= p.len;
					return readChunk(chunk_re,api,buf.sub(p.len,len),len);
				}
			}
			// prevent buffer accumulation
			if( len > 10 ) return false;
			chunk_buf = buf.sub(0,len);
			return true;
		}

		if( chunk_size > len ) {
			chunk_size -= len;
			api.writeBytes(buf,0,len);
			return true;
		}
		var end = chunk_size + 2;
		if( len >= end ) {
			if( chunk_size > 0 )
				api.writeBytes(buf,0,chunk_size);
			len -= end;
			chunk_size = null;
			if( len == 0 )
				return true;
			return readChunk(chunk_re,api,buf.sub(end,len),len);
		}
		if( chunk_size > 0 )
			api.writeBytes(buf,0,chunk_size);
		chunk_size -= len;
		return true;
	}


    //Socket Callbacks
    function on_socket_connected( do_post:Bool ) {
      this.onOpen(); // we have an open connection
      customRequest(do_post);
    }

    function on_socket_write_done() { sock.read(); }
    function on_socket_error( ecode:Int, emsg:String ) {
      switch( ecode ) {
        case 1:
          sock.close();
          sock = null;
          onClose();
        default:
          onErrorFull(ecode,emsg);
      }
    }

    // dynamic function should for callbacks
    public dynamic function onOpen() { }
    public dynamic function onClose() { }
	public dynamic function onData( data : String ) { }
	public dynamic function onError( msg : String ) { }
    public dynamic function onErrorFull( code:Int, msg : String ) { onError(msg); }
	public dynamic function onStatus( status : Int ) { }
}
